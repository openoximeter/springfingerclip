**The OpenOximeter is currently in an early prototype stage. It is never safe to build your own medical equipment. This project is open to encourage experts to work together to build the best and most adaptable solution.**

## Finger clip

The finger clip is drawn using OpenSCAD, you can download OpenSCAD from [here](https://www.openscad.org/downloads.html). If you are new to using OpenSCAD there is a really nice [Cheat Sheet](https://www.openscad.org/cheatsheet/) which gives you and overview of all the commands.

The file is parametric so that the size of element in the design can be controlled by editing the parameters at the start of the file.

### Mechanical BOM

**Spring**

I have been using a spring from the clothes pegs I had at home. They were originally from Sainsbury's, they are probably [these ones](https://www.sainsburys.co.uk/gol-ui/product/all-utility-room/sainsburys-home-soft-grip-pegs-x24-131455988-p) (though mine are a different colour).

**Screws**

No2 (2.2 mm) x 4.5 mm pozi pan self tapping screws. Qantity 8.


### STLs

The STLs are ignored by this repository as they fill up the git history with tonnes of old models. Use OpenSCAD to generate the STLs.

### Printing

Recommended print settings are 0.2mm layer height (or lower). With no supports, but with a brim. Test have been performed with a Prusa i3 MK3.

### Assembly

The mechanical assembly is as shown below:  
![](ExplodedRender.png)  
![](Render.png)
